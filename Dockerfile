FROM alpine:3.14

WORKDIR /deploy

ARG KUBECTL_VERSION=1.17.9
ARG HELM_VERSION=3.12.2

RUN apk add bash curl git openssh

RUN curl -SsL https://get.helm.sh/helm-v$HELM_VERSION-linux-amd64.tar.gz -o helm.tar.gz && \
    curl -SsL https://get.helm.sh/helm-v$HELM_VERSION-linux-amd64.tar.gz.sha256 -o helm.tar.gz.sha256 && \
    echo "$(cat helm.tar.gz.sha256)  helm.tar.gz" | sha256sum -c && \
    tar xvzf helm.tar.gz && \
    install -o root -g root -m 0755 linux-amd64/helm /usr/local/bin/helm && \
    rm -rf linux-amd64 helm.tar.gz* && \
    helm version --client
RUN curl -SsLO https://dl.k8s.io/release/v$KUBECTL_VERSION/bin/linux/amd64/kubectl && \
    curl -SsLO https://dl.k8s.io/release/v$KUBECTL_VERSION/bin/linux/amd64/kubectl.sha256 && \
    echo "$(cat kubectl.sha256)  kubectl" | sha256sum -c && \
    install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl && \
    rm kubectl* && \
    kubectl version --client
