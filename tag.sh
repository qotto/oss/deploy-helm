#!/bin/sh

ALPINE_VERSION=$(cat Dockerfile | grep FROM | head -n 1 | sed 's/.*://' )
KUBECTL_VERSION=$(cat Dockerfile | grep 'ARG KUBECTL_VERSION' | head -n 1 | sed 's/.*=//' )
HELM_VERSION=$(cat Dockerfile | grep 'ARG HELM_VERSION' | head -n 1 | sed 's/.*=//' )
FULL_TAG="alpine-$ALPINE_VERSION-kubectl-$KUBECTL_VERSION-helm-$HELM_VERSION"

echo "$FULL_TAG"
